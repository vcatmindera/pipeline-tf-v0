
variable "environmentName" {
  default = "VCM-testing"
}

## VPC

variable "aws_region" {
  description = "AWS region"
  default     = "eu-west-3"
}

variable "aws_azs" {
  description = "AWS AZs - Availability Zones"
  type    = list(string)
  default     = ["eu-west-3a", "eu-west-3b"]
}
